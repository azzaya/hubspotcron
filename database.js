const mysql = require('mysql');
const config = require('./config.json');

class Database {
    constructor() {
        this.connection = mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database
        });
    }

    query(sql, args) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err, rows) => {
                if (err) return reject(err);
                resolve(rows);
            });
        });
    }

    close() {
        return new Promise((resolve, reject) => {
           this.connection.end(err => {
               console.log(err);
               if (err) return reject(err);
               resolve();
           });
        });
    }
}

module.exports = Database;
