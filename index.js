var Hubspot = require("hubspot");
var Database = require("./database.js");
const request = require("request");
const logger = require('./logger');
const fs = require('fs');

// Database Connection
var db = new Database();
let result;
let logId;

async function getAll(log_id, company_id) {
  return Promise.all([
    db.query(
      "SELECT * FROM logProp INNER JOIN Property ON logProp.prop_id = Property.property_id WHERE logProp.log_id=? and property_name=?",
      [log_id, "callpro_number"]
    ),
    db.query(
      "SELECT * FROM logProp INNER JOIN Property ON logProp.prop_id = Property.property_id WHERE logProp.log_id=? and property_name=?",
      [log_id, "callpro_agent"]
    ),
    db.query(
      "SELECT * FROM logProp INNER JOIN Property ON logProp.prop_id = Property.property_id WHERE logProp.log_id=? and property_name=?",
      [log_id, "callpro_type"]
    ),
    db.query("SELECT * FROM token WHERE id=?", [company_id]),
    db.query("SELECT * FROM numbers WHERE company_id=?", [company_id]),
    db.query(
      "SELECT * FROM logProp INNER JOIN Property ON logProp.prop_id = Property.property_id WHERE logProp.log_id=? and property_name=?",
      [log_id, "callpro_ticket"]
    ),
    db.query(
      "SELECT * FROM logProp INNER JOIN Property ON logProp.prop_id = Property.property_id WHERE logProp.log_id=? and property_name=?",
      [log_id, "callpro_task"]
    ),
    db.query(
      "SELECT * FROM logProp INNER JOIN Property ON logProp.prop_id = Property.property_id WHERE logProp.log_id=? and property_name=?",
      [log_id, "callpro_press"]
    ),
  ]);
}

async function getLogs() {
  return db.query("SELECT * FROM log WHERE company_id = 68 and hubspot=? and failed=? LIMIT ?", [
    0,
    0,
    100,
  ]);
}
async function hubspotlog() {
  var tokens, numbers;
  const logs = await getLogs();
  let values;
  let callproNumber, callproAgent, callproType, owner;
  var i = 0;
  for (const log of logs) {
    try {
      logId = log.log_id;
      values = await getAll(log.log_id, log.company_id);
      let cpNumber = await values[0];
      let cpAgent = await values[1];
      let cpType = await values[2];
      let cpTicket = await values[5];
      let cpTask = await values[6];
      let cpPress = await values[7];
      if (cpNumber.length != 0 && cpAgent.length != 0 && cpType.length != 0) {
        callproNumber = await values[0][0].value; // callpro_number
        callproAgent = await values[1][0].value; // callpro_agent
        callproType = await values[2][0].value; // callpro_type
        tokens = await values[3][0]; // tokens
        numbers = await values[4]; // numbers

        if (cpTicket.length != 0) {
          cpTicket = await values[5][0].value; // callpro_ticket
        }
        if (cpTask.length != 0) {
          cpTask = await values[6][0].value; // callpro_task
        }
        if (cpPress.length != 0) {
          cpPress = await values[7][0].value; // callpro_task
        }
        owner = await getOwner(numbers, callproType, tokens.agentName);

        // START HUBSPOT
        const hubspot = new Hubspot({
          clientId: "4c41f545-c6a4-4680-91ae-61d2d12f3937",
          clientSecret: "0deafc2d-33ba-47ac-908a-9e2cedef6b31",
          redirectUri:
            "https://ap-east-1.console.aws.amazon.com/lambda/home?region=ap-east-1#/functions/HubspotCreateContacts?tab=configuration",
          refreshToken: tokens.token,
        });

        try {
          await hubspot.refreshAccessToken();
        } catch(err) {
          logger.error("ACCESS TOKEN ERROR", err)
          await db.query("INSERT INTO hb_error (log_id, error_type, error_message, created_at) VALUES(?, ?, ?, NOW())", [logId, "Refresh access token", err.message]);
          await db.query("UPDATE log SET failed = 1 WHERE log_id = ?", [logId]);
          continue;
        }

        let vid = [];
        let contactObj;
        var respCont;
        try {
          respCont = await hubspot.contacts.search(callproNumber);
        } catch(err) {
          logger.error("HUBSPOT CONTACTS SEARCH ERROR ", err)
          await db.query("INSERT INTO hb_error (log_id, error_type, error_message, created_at) VALUES(?, ?, ?, NOW())", [logId, "Hubspot contacts search", err.message]);
          await db.query("UPDATE log SET failed = 1 WHERE log_id = ?", [logId]);
          continue;
        }
        console.log("enig hewlej uzej baina ", respCont);
        if (respCont["contacts"].length == 0) {
          logger.info("ene dugaar baihgui tul shineer uusgeh oroldlogo ", callproNumber)
          contzctObj = {
            properties: [
              { property: "firstname", value: callproNumber },
              { property: "lastname", value: "CallPro" },
              { property: "phone", value: callproNumber },
              { property: "callpro_new_call", value: "true" },
            ],
          };

          var respCreated;
          // create contact
          try {
            respCreated = await hubspot.contacts.create(contactObj);
          } catch(err) {
            logger.error('HUBSPOT CONTACTS CREATE ERROR', err)
            await db.query("INSERT INTO hb_error (log_id, error_type, error_message, created_at) VALUES(?, ?, ?, NOW())", [logId, "Hubspot contacts create", err.message]);
            await db.query("UPDATE log SET failed = 1 WHERE log_id = ?", [logId]);
            continue;
          }
          await hubspot.contacts
            .update(vid[0], contactUpdate)
            .then(async () => {
              await db.query("UPDATE log SET hubspot = 1 WHERE log_id = ?", [
                log.log_id,
              ]);
            })
            .catch((err) => console.log(err));
        } 
      } else {
        logger.error("aldaa garsan");
        var fileName = 'record_' + log.unique_id + ".wav";
        var filePath = './records/' + fileName
        var file = fs.createWriteStream(filePath);
        
        await db.query("UPDATE log SET failed = 1 WHERE log_id = ?", [
          log.log_id,
        ]);

        var logContent = 'aldaa garsan ID ' + log.log_id;

        fs.appendFile('./internship.log', logContent, err => {
          if (err) {
            console.error(err)
            return
          }
        })
      }
      i++;
    } catch(err) {
      logger.error("BIG ERROR", err);
      var propertiesObject = {
          key:'8KRzM5HJMm7u5xtEC5cdk1rpXzDHSE17nCvE1FYh',
          from:'137070',
          to: '90007071',
          text: 'BIG ERROR log_id: ' + log.log_id + 'error: ' + err.message
      };
      await request({ url: 'https://api.messagepro.mn/send', 'json': true, qs: propertiesObject }, (error, response) => {
        if (error) {
          logger.error('MessagePro API not work');
        } else {
          logger.info(response);
        }
      });
      await db.query("INSERT INTO hb_error (log_id, error_type, error_message, created_at) VALUES(?, ?, ?, NOW())", [logId, "BIG ERROR", err.message]);
      await db.query("UPDATE log SET failed = 1 WHERE log_id = ?", [
        log.log_id,
      ]);
    }
  }
result = i;
process.exit(1);
}
hubspotlog();
