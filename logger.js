const winston = require('winston')
require('winston-daily-rotate-file')

logFormat = winston.format.printf(info => {
    const formattedDate = info.timestamp.replace('T', ' ').replace('Z', '');
    return `${formattedDate} | ${info.message} | ${info.level}`
})

const options = {
    file: {
        level: 'info',
        filename: './logs/hubspot-error-%DATE%.log',
        handleExceptions: true,
        zippedArchive: false,
        json: true,
        maxsize: 5242880, //5MB
        maxFiles: 5,
        colorize: false,
    }
};

const logger = winston.createLogger({
    levels: winston.config.npm.levels,
    format: winston.format.combine(
        winston.format.timestamp(),
        logFormat
    ),
    transports: [
        new winston.transports.DailyRotateFile(options.file)
    ],
    exitOnError: false
})

module.exports = logger